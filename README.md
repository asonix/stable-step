# stable-step
_steps in stable rust_

## Examples
### No Dependencies
```toml
[dependencies]
stable-step = "0.1"
```

```rust
use stable_step::{Step, StepExt};

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
enum MyEnum {
    A,
    B,
}

impl Step for MyEnum {
    const MIN: Self = Self::A;
    const MAX: Self = Self::B;

    fn next(&self) -> Option<Self> {
        match self {
            Self::A => Some(Self::B),
            _ => None,
        }
    }

    fn prev(&self) -> Option<Self> {
        match self {
            Self::B => Some(Self::A),
            _ => None,
        }
    }
}

fn main() {
    for value in MyEnum::iter() {
        println!("{:?}", value);
    }
}
```

### Proc Macros
```toml
[dependencies]
stable-step = { version = "0.1", features = ["derive"] }
```

```rust
use stable_step::{Step, StepExt};

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Step)]
enum MyEnum {
    A,
    B,
}

fn main() {
    for value in MyEnum::iter() {
        println!("{:?}", value);
    }
}
```

## License

<sup>
Licensed under either of <a href="https://git.asonix.dog/asonix/stable-step/src/branch/main/LICENSE-APACHE">Apache License, Version
2.0</a> or <a href="https://git.asonix.dog/asonix/stable-step/src/branch/main/LICENSE-MIT">MIT license</a> at your option.
</sup>

<br/>

<sub>
Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in this crate by you, as defined in the Apache-2.0 license, shall
be dual licensed as above, without any additional terms or conditions.
</sub>
