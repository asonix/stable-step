use proc_macro::{self, TokenStream};
use quote::quote;
use syn::{parse_macro_input, Data, DeriveInput};

#[proc_macro_derive(Step)]
pub fn derive(input: TokenStream) -> TokenStream {
    let DeriveInput {
        ident,
        generics,
        data,
        ..
    } = parse_macro_input!(input);

    let data = if let Data::Enum(data) = data {
        data
    } else {
        panic!("Step can only be derived for Enums");
    };

    for variant in &data.variants {
        if !variant.fields.is_empty() {
            panic!("Step can only be derived for Enums with no variant fields");
        }
    }

    if data.variants.is_empty() {
        panic!("Step cannot be derived for an empty Enum");
    }

    let first = data.variants.iter().next().unwrap();
    let last = data.variants.iter().last().unwrap();

    let nexts: proc_macro2::TokenStream = data
        .variants
        .iter()
        .zip(data.variants.iter().skip(1))
        .map(|(lesser, greater)| {
            let lesser = &lesser.ident;
            let greater = &greater.ident;
            quote! {
                Self::#lesser => Some(Self::#greater),
            }
        })
        .collect();

    let prevs: proc_macro2::TokenStream = data
        .variants
        .iter()
        .zip(data.variants.iter().skip(1))
        .map(|(lesser, greater)| {
            let lesser = &lesser.ident;
            let greater = &greater.ident;
            quote! {
                Self::#greater => Some(Self::#lesser),
            }
        })
        .collect();

    let (impl_generics, type_generics, where_clause) = generics.split_for_impl();

    let output = quote! {
        impl #impl_generics ::stable_step::Step for #ident #type_generics #where_clause {
            const MIN: Self = Self::#first;
            const MAX: Self = Self::#last;

            fn next(&self) -> Option<Self>
            where
                Self: Sized,
            {
                match self {
                    Self::#last => None,
                    #nexts
                }
            }

            fn prev(&self) -> Option<Self>
            where
                Self: Sized,
            {
                match self {
                    Self::#first => None,
                    #prevs
                }
            }
        }
    };

    output.into()
}
