use stable_step::{Step, StepExt};

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
enum MyEnum {
    A,
    B,
    C,
    D,
    E,
    F,
}

impl Step for MyEnum {
    const MIN: Self = Self::A;
    const MAX: Self = Self::F;

    fn next(&self) -> Option<Self>
    where
        Self: Sized,
    {
        match self {
            Self::A => Some(Self::B),
            Self::B => Some(Self::C),
            Self::C => Some(Self::D),
            Self::D => Some(Self::E),
            Self::E => Some(Self::F),
            Self::F => None,
        }
    }

    fn prev(&self) -> Option<Self>
    where
        Self: Sized,
    {
        match self {
            Self::A => None,
            Self::B => Some(Self::A),
            Self::C => Some(Self::B),
            Self::D => Some(Self::C),
            Self::E => Some(Self::D),
            Self::F => Some(Self::E),
        }
    }
}

fn main() {
    println!("All");
    for value in MyEnum::iter() {
        println!("{:?}", value);
    }

    println!("Subset");
    for value in MyEnum::B.iter_to(MyEnum::E) {
        println!("{:?}", value);
    }

    println!("Reversed");
    for value in MyEnum::B.iter_to_inclusive(MyEnum::E).rev() {
        println!("{:?}", value);
    }
}
