use stable_step::StepExt;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, stable_step::Step)]
enum MyEnum {
    A,
    B,
    C,
    D,
    E,
    F,
}

fn main() {
    println!("All");
    for value in MyEnum::iter() {
        println!("{:?}", value);
    }

    println!("Subset");
    for value in MyEnum::B.iter_to(MyEnum::E) {
        println!("{:?}", value);
    }

    println!("Reversed");
    for value in MyEnum::B.iter_to_inclusive(MyEnum::E).rev() {
        println!("{:?}", value);
    }
}
